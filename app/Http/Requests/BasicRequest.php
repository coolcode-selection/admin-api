<?php namespace App\Http\Requests;

use http\Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

abstract class BasicRequest extends FormRequest
{

  public function defaultValidateFormData()
  {
    $validationData = $this->all();
    return $validationData;
  }


  protected function validationData()
  {
    return $this->defaultValidateFormData();
  }

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  protected function failedValidation(Validator $validator)
  {
    throw new HttpResponseException(response()->json($validator->errors(), 422));
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public abstract function rules(Request $request);
}