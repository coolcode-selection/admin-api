<?php namespace App\Utils;

/**
 * @author <a href="mailto:smartydroid.com@gmail.com">Smartydroid</a>
 */
final class Util
{

  public static function isNull($text)
  {
    return is_null($text) || strlen(trim($text)) <= 0;
  }

}