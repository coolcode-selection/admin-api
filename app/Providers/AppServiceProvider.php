<?php

namespace App\Providers;

use Api\StarterKit\Serializer\ApiSerializer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Zizaco\Entrust\MigrationCommand;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    Schema::defaultStringLength(191);
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    if (config('api.transformer')) {
      $this->app['api.transformer']
        ->getAdapter()
        ->getFractal()
        ->setSerializer(new ApiSerializer);
    }
    // entrust 未适配 laravel 5.5 临时 fix 一下，否则无法执行 entrust:migration
    $this->app->extend('command.entrust.migration', function () {
      return new class extends MigrationCommand {
        public function handle()
        {
          parent::fire();
        }
      };
    });
  }
}
