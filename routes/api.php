<?php

use Illuminate\Http\Request;

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
  'namespace'  => 'App\Http\Controllers',
  'middleware' => 'cors'
], function ($api) {
   $api->get('/', function (){
     return app()->version();
   });

  $api->group([
    'middleware' => 'jwt.auth',
  ], function ($api) {


  });

});